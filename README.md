# ESCO Analysis

This project contains an analysis of the [ESCO service](https://esco.ec.europa.eu/). We used python within jupyter notebooks to make some sample queries, so that 
you can get a better idea of what the service returns and how requests are created. If you want to contribute to this analysis please do so!

# Requirements

1. Install `python` (including `pip`) and `docker`

2. Install and run `jupyter-lab`
 

    pip install jupyterlab
    jupyter-lab


4. Pull and run ESCO Docker container 


    docker pull docker pull appslabhub/esco:v1.1.0
    docker run -p 8080:8080 appslabhub/esco:v1.1.0


# Knows Issues 

When running scripts you might want to increase the heap of you JVM by using the following ENVIRONMENT variables:

    CATALINA_OPTS="-Xmx5g -Xms5g"
    
    JVM_OPTS="-Xmx5g -Xms5g"
